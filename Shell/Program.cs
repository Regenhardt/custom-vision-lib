﻿namespace Shell
{
	using System;
	using System.IO;
	using System.Threading.Tasks;
	using CustomVisionLib;

	public class Program
	{
		private static readonly string DownloadFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads",
			"CV-Images");

		private static string endpoint = "https://my-example-ai-service.cognitiveservices.azure.com";

		public static async Task Main(string[] args)
		{
			// endpoint projectId trainingKey imageId/command
			if (args.Length < 3)
			{
				Console.WriteLine("Need at least 3 args separated by spaces: endpoint projectId trainingKey");
				Console.WriteLine(
					$"Usage:{Environment.NewLine}" +
					"Shell.exe {endpoint} {projectID} {trainingKey} [command]" + Environment.NewLine +
					$"Example endpoint: {endpoint}" +
					$"{Environment.NewLine}{Environment.NewLine}" +
					$"Without commands, it just counts existing images.{Environment.NewLine}" +
					$"Commands:{Environment.NewLine}" +
					$"{"stats:",-10}Checks how many images there are in the project and how much total size they have.{Environment.NewLine}" +
					$"{"download:",-10}Downloads all images to {DownloadFolder}");
				return;
			}

			endpoint = args[0];
			var projectId = Guid.TryParse(args[1], out var guid) ? guid : throw new FormatException("First parameter must be a project id in the form of a GUID.");
			var trainingKey = args[2];

			var service = new CustomVisionService(endpoint, projectId, trainingKey);
			service.OnProgress += msg => Console.WriteLine(msg);

			if (args.Length == 3)
			{
				var count = await service.CountImages();
				Console.WriteLine($"{count} images found");
				return;
			}

			if (args.Length > 3 && args[3] == "stats")
			{
				await service.GetStats();
				return;
			}

			if (Guid.TryParse(args[3], out var imageId))
			{
				await service.AnalyseImage(imageId);
				return;
			}

			if (args[3] == "download")
			{
				await service.DownloadImages(DownloadFolder);
				return;
			}
		}
	}
}