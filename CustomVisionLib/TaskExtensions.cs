﻿namespace CustomVisionLib
{
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;

	internal static class TaskExtensions
	{
		public static Task<T[]> WhenAll<T>(this IEnumerable<Task<T>> tasks) => Task.WhenAll(tasks);

		public static Task WhenAll(this ParallelQuery<Task> tasks) => Task.WhenAll(tasks);
	}
}
